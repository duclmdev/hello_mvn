package com.hello.mvn;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hello.mvn.entity.Course;
import com.hello.mvn.entity.Student;
import com.hello.mvn.entity.StudentCourseDetail;
import com.hello.mvn.expression.Criteria;
import com.hello.mvn.expression.operator.Operator;
import com.hello.mvn.model.Nationality;
import com.hello.mvn.model.Person;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

@Slf4j
class CriteriaTest {

    @Test
    void shouldAnswerWithTrue() {
        assertTrue(true);
    }

    @Test
    void testPerson() throws JsonProcessingException {
        // (name = "Lewis" and (age > 20 or nationality = VN)) or (name = "Nathan" and age < 30)
        //language=json
        var expStr = """
                {
                    "expression": [
                        {"type": "OPEN"},
                        {"type": "COMPARE", "name": "name", "operator": "==", "value": "Lewis"},
                        {"type": "AND"},
                        {"type": "OPEN"},
                        {"type": "COMPARE", "name": "age", "operator": ">", "value": "20"},
                        {"type": "OR"},
                        {"type": "COMPARE", "name": "nationality", "operator": "==", "value": "VN"},
                        {"type": "CLOSE"},
                        {"type": "CLOSE"},
                        {"type": "OR"},
                        {"type": "OPEN"},
                        {"type": "COMPARE", "name": "name", "operator": "==", "value": "Nathan"},
                        {"type": "AND"},
                        {"type": "COMPARE", "name": "age", "operator": "<", "value": "30"},
                        {"type": "CLOSE"}
                    ]
                }
                """;
        var mapper = new ObjectMapper();
        var expression = mapper.readValue(expStr, Criteria.class);

        // log.info("sql =\n{}", expression.toSql(Person.class));
        var exp = expression.toExpression(Person.class);
        System.out.println("sql =\n" + expression.toSql(Person.class));

        System.out.println("----->");
        var person = new Person().name("Lewis").age(18).nationality(Nationality.VN);
        SpELTest.tryEvaluate(person, exp);
    }

    @Test
    void testStudent() throws JsonProcessingException {
        //exp = (student.name.contains("Lewis") and rating >= 5) or (course.name == "CS" and rating > 6)
        //sql = (s.name LIKE CONCAT('%', 'Lewis', '%') and scd.rating >= 5.0) or (c.name = 'CS' and scd.rating < 6.0)
        //language=json
        var expStr = """
                {
                    "expression": [
                        {"type": "OPEN"},
                        {"type": "COMPARE", "name": "student.name", "operator": "CONTAINS", "value": "Lewis"},
                        {"type": "AND"},
                        {"type": "COMPARE", "name": "rating", "operator": ">=", "value": "5"},
                        {"type": "CLOSE"},
                        {"type": "OR"},
                        {"type": "OPEN"},
                        {"type": "COMPARE", "name": "course.name", "operator": "==", "value": "CS"},
                        {"type": "AND"},
                        {"type": "COMPARE", "name": "rating", "operator": "<", "value": "6"},
                        {"type": "CLOSE"}
                    ]
                }
                """;
        var mapper = new ObjectMapper();
        var expression = mapper.readValue(expStr, Criteria.class);

        var exp = expression.toExpression(StudentCourseDetail.class);
        var sql = expression.toSql(StudentCourseDetail.class);
        System.out.println("exp =\n" + exp);
        System.out.println("sql =\n" + sql);

        System.out.println("----->");
        var student = Student.builder().name("Lewitt").build();
        var course = Course.builder().name("algorithm").build();
        var studentCourse = StudentCourseDetail.builder()
                .student(student)
                .course(course)
                .rating(5)
                .build();
        SpELTest.tryEvaluate(studentCourse, exp);
    }

    @Test
    void testOperator() {
        var contains = Operator.of("+");
        System.out.println(contains);

        System.out.println(Integer.class.isAssignableFrom(int.class));
        System.out.println(int.class.isAssignableFrom(Integer.class));

        System.out.println(Number.class.isAssignableFrom(int.class));
        System.out.println(Number.class.isAssignableFrom(Double.class));
        System.out.println(int.class.isAssignableFrom(Number.class));
    }
}
