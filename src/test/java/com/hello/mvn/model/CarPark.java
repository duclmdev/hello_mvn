package com.hello.mvn.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CarPark {

    private List<Car> cars = new ArrayList<>();
}
