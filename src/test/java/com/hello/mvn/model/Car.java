package com.hello.mvn.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Car {

    private String make;
    private String model;
    private Engine engine;
    private int yearOfProduction;
}
