package com.hello.mvn.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Engine {

    private String name;
    private int capacity;
    private int horsePower;
    private int numberOfCylinders;
}
