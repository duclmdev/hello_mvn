package com.hello.mvn.model;

import jakarta.persistence.Column;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
public class Person {

    @Column(name = "person_name")
    private String name;

    @Column(name = "person_age")
    private int age;

    @Column(name = "person_nationality")
    private Nationality nationality;
}
