package com.hello.mvn.utils;

import jakarta.persistence.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

class PropertyMappingTest {

    public static class ParentEntity {

        @Column
        private int age;
    }

    public static class ChildEntity extends ParentEntity {

        @Id
        private int id;

        @Column
        private String name;

        @OneToMany
        private List<Contact> phoneNumbers;

        @OneToOne
        private String test;

        @ManyToOne
        private String group;
    }

    private static class Contact {

        @Column
        private String phoneNumber;
    }

    @BeforeEach
    void setUp() {
    }

    @Test
    void testLoad() {
        var providerProps = PropertyMapping.load(ChildEntity.class);
        PropertyMapping.load(ParentEntity.class);
        System.out.println(providerProps);
        System.out.println();
        printMappingCache();
    }

    private static void printMappingCache() {
        PropertyMapping.getCache().forEach((k, v) -> {
            System.out.println(k + " -> ");
            v.propertyMap().forEach((name, value) -> System.out.printf(" %15s: %s\n", name, value));
        });
    }
}
