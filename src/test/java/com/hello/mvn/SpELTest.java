package com.hello.mvn;

import com.hello.mvn.model.*;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.expression.spel.SpelParserConfiguration;
import org.springframework.expression.spel.standard.SpelExpressionParser;

import java.util.Map;

@Slf4j
class SpELTest {

    @Test
    void testNormal() {
        var engine = Engine.builder()
                .name("v8")
                .capacity(1000)
                .horsePower(500)
                .numberOfCylinders(8)
                .build();

        var car = Car.builder()
                .make("Good manufacturer")
                .model("Model 3")
                .yearOfProduction(2014)
                .engine(engine)
                .build();

        CarPark carPark = new CarPark();
        carPark.getCars().add(car);

        var testcases = Map.of(
                "engine.name == 'v8'", car, //
                "engine.capacity == 1000", car, //
                "cars[0].model == 'Model 3'", carPark, //
                "cars[0].make == 'Good manufacturer'", carPark//
        );
        testcases.forEach((exp, context) -> {
            tryEvaluate(context, exp);
        });
    }

    @Test
    void testWithObject() {
        String[] expressions = new String[]{
                "nationality",
                "name == 'Lewis' and age * 18 and nationality.name eq 'VN'",
                "name == 'Lewis' and age == 18 and nationality == 'VN'",
                "(name == 20 and (age > 'Lewis' || nationality eq 'SG')) or (name == 'Nathan' and age < 30)"
        };
        var person = new Person().name("Lewis").age(18).nationality(Nationality.VN);

        for (var exp : expressions) {
            tryEvaluate(person, exp);
        }
    }

    static void tryEvaluate(Object context, String exp) {
        var config = new SpelParserConfiguration(true, true);
        var expression = new SpelExpressionParser(config).parseExpression(exp);

        System.out.println("exp = \n" + exp);
        System.out.println("type = " + expression.getValueType(context));
        System.out.println("value = " + expression.getValue(context));
        System.out.println("-----------------");
    }
}
