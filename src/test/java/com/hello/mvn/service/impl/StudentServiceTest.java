package com.hello.mvn.service.impl;

import com.hello.mvn.entity.Student;
import com.hello.mvn.exception.ServiceException;
import com.hello.mvn.repository.StudentRepository;
import com.hello.mvn.service.StudentService;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class StudentServiceTest {

    @Mock
    private StudentRepository repository;

    @InjectMocks
    private StudentService service;

    private Student student;

    @BeforeAll
    static void init() {
        // repository = mock(StudentRepository.class);
        // service = mock(StudentService.class);
    }

    @BeforeEach
    void setUp() {
        student = new Student();
        student.setId(1);
        student.setName("John");
    }

    @Test
    @DisplayName("Should throw ServiceException when null entity is provided")
    void createWhenNullEntityIsProvidedThenThrowServiceException() {
        assertThrows(ServiceException.class, () -> {
            service.create(null);
        });
    }

    @Test
    @DisplayName("Should throw ServiceException when entity with existing ID is provided")
    void createWhenEntityWithExistingIdIsProvidedThenThrowServiceException() {
        when(repository.existsById(student.getId())).thenReturn(true);

        assertThrows(ServiceException.class, () -> service.create(student));

        verify(repository, never()).save(student);
    }

    @Test
    @DisplayName("Should create and return the entity when valid entity is provided")
    void createWhenValidEntityIsProvided() throws ServiceException {
        when(repository.save(student)).thenReturn(student);

        Student createdStudent = service.create(student);

        assertNotNull(createdStudent);
        assertEquals(student.getId(), createdStudent.getId());
        assertEquals(student.getName(), createdStudent.getName());

        verify(repository, times(1)).save(student);
    }
}
