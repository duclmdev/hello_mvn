package com.hello.mvn.expression;

import com.hello.mvn.utils.PropertyMapping;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Or implements Block {

    @Override
    public String toSql(PropertyMapping mapping) {
        return " OR ";
    }

    @Override
    public String toExpression(PropertyMapping mapping) {
        return " || ";
    }
}
