package com.hello.mvn.expression;

import com.hello.mvn.utils.PropertyMapping;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class OpenParenthesis implements Block {

    @Override
    public String toSql(PropertyMapping mapping) {
        return "(";
    }

    @Override
    public String toExpression(PropertyMapping mapping) {
        return "(";
    }
}
