package com.hello.mvn.expression.operator;

import com.hello.mvn.utils.Property;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class StringContains implements Operator {

    public static final Operator INSTANCE = new StringContains();

    @Override
    public String toSql(String name, String value) {
        return String.format("%s LIKE CONCAT('%%', %s, '%%')", name, value);
    }

    @Override
    public String toExpression(String name, String value) {
        return String.format("%s.contains(%s)", name, value);
    }

    @Override
    public boolean canApplyTo(Property.Type type) {
        return type == Property.Type.STRING;
    }
}
