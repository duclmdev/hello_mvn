package com.hello.mvn.expression.operator;

import com.hello.mvn.utils.Property;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class NumberMultiply implements Operator {

    public static final Operator INSTANCE = new NumberMultiply();

    @Override
    public String toExpression(String name, String value) {
        return String.format("%s * %s", name, value);
    }

    @Override
    public boolean canApplyTo(Property.Type type) {
        return type == Property.Type.NUMBER;
    }
}
