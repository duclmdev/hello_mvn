package com.hello.mvn.expression.operator;

import com.hello.mvn.utils.Property;

public interface Operator {

    default String toSql(String name, String value) {
        return toExpression(name, value);
    }

    String toExpression(String name, String value);

    boolean canApplyTo(Property.Type type);

    default Operator applyTo(Property.Type type) {
        if (canApplyTo(type)) return this;
        throw new RuntimeException("Cannot apply to " + type.name());
    }

    static Operator of(String op) {
        return switch (op) {
            case "+" -> NumberAdd.INSTANCE;
            case "-" -> NumberSubtract.INSTANCE;
            case "*" -> NumberMultiply.INSTANCE;
            case "/" -> NumberDivide.INSTANCE;
            case ">" -> NumberGreaterThan.INSTANCE;
            case "<" -> NumberLessThan.INSTANCE;
            case ">=" -> NumberGreaterThanOrEquals.INSTANCE;
            case "<=" -> NumberLessThanOrEquals.INSTANCE;
            case "==" -> Equals.INSTANCE;
            case "!=" -> NotEquals.INSTANCE;
            case "CONTAINS" -> StringContains.INSTANCE;
            default -> throw new RuntimeException("Operator '" + op + "' is not supported!");
        };
    }
}
