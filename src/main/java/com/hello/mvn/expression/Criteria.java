package com.hello.mvn.expression;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hello.mvn.utils.PropertyMapping;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Criteria {

    @JsonProperty("expression")
    private List<Block> blocks;

    public String toSql(Class<?> entityClass) {
        var mapping = PropertyMapping.load(entityClass);
        var sb = new StringBuilder();
        for (var block : blocks) {
            sb.append(block.toSql(mapping));
        }
        return sb.toString();
    }

    public String toExpression(Class<?> entityClass) {
        var mapping = PropertyMapping.load(entityClass);
        var sb = new StringBuilder();
        for (var block : blocks) {
            sb.append(block.toExpression(mapping));
        }
        return sb.toString();
    }
}
