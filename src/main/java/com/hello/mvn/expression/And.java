package com.hello.mvn.expression;

import com.hello.mvn.utils.PropertyMapping;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class And implements Block {

    @Override
    public String toSql(PropertyMapping mapping) {
        return " AND ";
    }

    @Override
    public String toExpression(PropertyMapping mapping) {
        return " && ";
    }
}
