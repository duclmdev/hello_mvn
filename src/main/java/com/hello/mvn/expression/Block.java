package com.hello.mvn.expression;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.hello.mvn.utils.PropertyMapping;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = OpenParenthesis.class, name = "OPEN"),
        @JsonSubTypes.Type(value = CloseParenthesis.class, name = "CLOSE"),
        @JsonSubTypes.Type(value = And.class, name = "AND"),
        @JsonSubTypes.Type(value = Or.class, name = "OR"),
        @JsonSubTypes.Type(value = Comparison.class, name = "COMPARE"),
        @JsonSubTypes.Type(value = Computation.class, name = "COMPUTE"),
})
public interface Block {

    String toSql(PropertyMapping mapping);

    String toExpression(PropertyMapping mapping);
}
