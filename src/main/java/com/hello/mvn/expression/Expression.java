package com.hello.mvn.expression;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hello.mvn.expression.operator.Operator;
import com.hello.mvn.utils.Property;
import com.hello.mvn.utils.PropertyMapping;
import lombok.Data;

@Data
public abstract class Expression implements Block {

    @JsonIgnore
    protected Property.Type type;

    protected String name;
    protected String operator;
    protected String value;

    @Override
    public String toExpression(PropertyMapping mapping) {
        var field = getField(mapping);
        var fieldType = field.type();
        var op = getOperator(fieldType);

        return switch (fieldType) {
            case NUMBER -> op.toExpression(name, String.valueOf(Double.parseDouble(value)));
            case STRING -> op.toExpression(name, String.format("'%s'", value));
            case OPTION -> op.toExpression(name + ".name", String.format("'%s'", value));
        };
    }

    @Override
    public String toSql(PropertyMapping mapping) {
        var field = getField(mapping);
        var fieldType = field.type();
        var op = getOperator(fieldType);

        var alias = field.tableAlias() == null ? "" : field.tableAlias() + ".";
        var column = alias + field.column();
        return switch (fieldType) {
            case NUMBER -> op.toSql(column, String.valueOf(Double.parseDouble(value)));
            case STRING -> op.toSql(column, String.format("'%s'", value));
            case OPTION -> op.toSql(column, String.format("'%s'", value));
        };
    }

    protected Operator getOperator(Property.Type fieldType) {
        return Operator.of(operator).applyTo(fieldType);
    }

    protected Property getField(PropertyMapping mapping) {
        var field = mapping.get(name);
        if (field != null) return field;

        var property = getReference(mapping);
        if (property != null) {
            return property;
        }
        throw new RuntimeException("Field '" + name + "' does not exist in " + mapping.className());
    }

    private Property getReference(PropertyMapping mapping) {
        var fields = name.split("\\s*\\.\\s*");
        var map = mapping;
        Property property = null;
        for (var f : fields) {
            property = map.get(f);
            map = PropertyMapping.get(property.clazz());
        }
        return property;
    }
}
