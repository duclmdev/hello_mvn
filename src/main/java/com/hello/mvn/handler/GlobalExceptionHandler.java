package com.hello.mvn.handler;

import com.hello.mvn.dto.response.ResponseDTO;
import com.hello.mvn.exception.ResourceNotFoundException;
import com.hello.mvn.exception.ServiceException;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ResponseDTO<ResponseDTO.EmptyBody>> resourceNotFoundException(
            ResourceNotFoundException ex, HttpServletRequest request
    ) {
        return generateResponse(ex.getMessage(), request, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({ServiceException.class})
    public ResponseEntity<ResponseDTO<ResponseDTO.EmptyBody>> serviceExceptionHandler(
            ServiceException ex, HttpServletRequest request
    ) {
        ex.getE().printStackTrace();
        return generateResponse(ex.getMessage(), request, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<ResponseDTO<ResponseDTO.EmptyBody>> globalExceptionHandler(
            Exception ex, HttpServletRequest request
    ) {
        ex.printStackTrace();
        return generateResponse(ex.getMessage(), request, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<ResponseDTO<ResponseDTO.EmptyBody>> generateResponse(
            String message, HttpServletRequest request, HttpStatus httpStatus
    ) {
        ResponseDTO.Status status = ResponseDTO.status(httpStatus)
                .setDetail(message)
                .setInstance(request.getRequestURI());
        return ResponseDTO.build(request, ResponseDTO.EmptyBody.INSTANCE, status).toResponseEntity();
    }
}
