package com.hello.mvn.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileOutputStream;

@Slf4j
@RestController
// @RequestMapping("")
@RequiredArgsConstructor
public class IndexController {

    @GetMapping
    public String getIndex() {
        return "OK!";
    }

    @GetMapping("/api")
    public String get() {
        return "OK!";
    }

    @PostMapping("/upload")
    public String handleFileUpload(
            @RequestPart("test") String test,
            @RequestPart("file") MultipartFile[] files
    ) throws JsonProcessingException {
        log.info("test: {}", test);
        for (var file : files) {
            try (var fos = new FileOutputStream(file.getOriginalFilename())) {
                fos.write(file.getBytes());
            } catch (Exception e) {
                return "not ok";
            }
        }
        return "ok";
    }
}
