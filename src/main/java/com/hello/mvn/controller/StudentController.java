package com.hello.mvn.controller;

import com.hello.mvn.dto.request.StudentRequestDTO;
import com.hello.mvn.dto.response.*;
import com.hello.mvn.entity.ActivationStatus;
import com.hello.mvn.entity.Student;
import com.hello.mvn.exception.ServiceException;
import com.hello.mvn.service.StudentService;
import com.hello.mvn.utils.PageableBuilder;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@Slf4j
@RestController
@RequestMapping("/students")
@RequiredArgsConstructor
public class StudentController {

    private final ModelMapper modelMapper = new ModelMapper();

    private final StudentService service;

    @GetMapping
    public ResponseEntity<ResponseDTO<PageDTO<StudentDTO>>> query(
            @RequestParam(value = "q", required = false) String query,
            @RequestParam(value = "status", required = false) Set<ActivationStatus> statusSet,
            @ParameterObject @ModelAttribute PageableBuilder pageable,
            HttpServletRequest request
    ) {
        var students = service.search(query, statusSet, pageable)
                .map(this::mapStudentDTO);
        return ResponseDTO.build(request, PageDTO.from(students)).toResponseEntity();
    }

    @PostMapping
    public ResponseEntity<ResponseDTO<StudentDTO>> create(
            @RequestBody StudentRequestDTO requestDTO, HttpServletRequest request
    ) throws ServiceException {
        var student = modelMapper.map(requestDTO, Student.class);
        var created = service.create(student);
        var studentDto = modelMapper.map(created, StudentDTO.class);
        return ResponseDTO.build(request, studentDto).toResponseEntity();
    }

    private StudentDTO mapStudentDTO(Student student) {
        var studentDTO = modelMapper.map(student, StudentDTO.class);
        studentDTO.setCourseList(mapCourseDTO(student));
        return studentDTO;
    }

    private List<CourseDTO> mapCourseDTO(Student student) {
        return student.getCourses()
                .stream()
                .map(detail -> modelMapper.map(detail.getCourse(), CourseDTO.class))
                .toList();
    }
}
