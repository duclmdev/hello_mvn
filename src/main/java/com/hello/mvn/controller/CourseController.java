package com.hello.mvn.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.hello.mvn.dto.request.CourseRequestDTO;
import com.hello.mvn.dto.response.*;
import com.hello.mvn.entity.ActivationStatus;
import com.hello.mvn.entity.Course;
import com.hello.mvn.entity.StudentCourseDetail;
import com.hello.mvn.exception.ResourceNotFoundException;
import com.hello.mvn.exception.ServiceException;
import com.hello.mvn.service.CourseService;
import com.hello.mvn.utils.PageableBuilder;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.annotation.PostConstruct;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RestController
@RequestMapping("/courses")
@RequiredArgsConstructor
public class CourseController {

    private final CourseService courseService;

    private final ModelMapper modelMapper = new ModelMapper();

    @PostConstruct
    public void init() {
    }

    @GetMapping
    @Operation(summary = "get courses")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "OK", content = {
            @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = ResponseDTO.class))})
    })
    @JsonView(Views.Public.class)
    public ResponseEntity<ResponseDTO<PageDTO<CourseDTO>>> query(
            @RequestParam(value = "q", required = false) String query,
            @RequestParam(value = "status", required = false) Set<ActivationStatus> statusSet,
            @ParameterObject @ModelAttribute PageableBuilder pageable,
            HttpServletRequest request
    ) {
        var dto = courseService.search(query, statusSet, pageable)
                .map(this::mapCourseDTO);
        log.info("query={}; status={}, paging={} => size={}", query, statusSet, pageable, dto.getNumberOfElements());
        return ResponseDTO.build(request, PageDTO.from(dto)).toResponseEntity();
    }

    @GetMapping("/{course_id}")
    public ResponseEntity<ResponseDTO<CourseDTO>> getCourseById(
            @PathVariable("course_id") Integer courseId, HttpServletRequest request
    ) throws ServiceException, ResourceNotFoundException {
        var courseDTO = mapCourseDTO(courseService.getById(courseId));
        return ResponseDTO.build(request, courseDTO).toResponseEntity();
    }

    @GetMapping("/{course_id}/students")
    public ResponseEntity<ResponseDTO<PageDTO<StudentDTO>>> getCourseStudents(
            @PathVariable("course_id") Integer courseId, @ParameterObject @ModelAttribute PageableBuilder pageable,
            HttpServletRequest request
    ) {
        var students = courseService.getStudents(courseId, pageable)
                .map(student -> modelMapper.map(student, StudentDTO.class));

        log.info("course id={}; paging={} => size={}", courseId, pageable, students.getNumberOfElements());
        return ResponseDTO.build(request, PageDTO.from(students)).toResponseEntity();
    }

    @PostMapping
    public ResponseEntity<ResponseDTO<CourseDTO>> create(
            @RequestBody CourseRequestDTO requestDTO, HttpServletRequest request
    ) throws ServiceException {
        var requestEntity = modelMapper.map(requestDTO, Course.class);
        var course = courseService.create(requestEntity);
        var courseDTO = modelMapper.map(course, CourseDTO.class);
        return ResponseDTO.build(request, courseDTO).toResponseEntity();
    }

    @PostMapping("/{course_id}/register/{student_id}")
    public ResponseEntity<ResponseDTO<StudentCourseDetailDTO>> register(
            @PathVariable("course_id") Integer courseId, @PathVariable("student_id") Integer studentId,
            HttpServletRequest request
    ) {
        // var detail = studentCourseService.register(courseId, studentId);
        // var detailDTO = modelMapper.map(detail, StudentCourseDetailDTO.class);
        // return ResponseDTO.build(request, detailDTO).toResponseEntity();
        return null;
    }

    private List<StudentDTO> convert(List<StudentCourseDetail> users) {
        var list = new ArrayList<StudentDTO>();
        for (var e : users) {
            list.add(modelMapper.map(e.getStudent(), StudentDTO.class));
        }
        return list;
    }

    private CourseDTO mapCourseDTO(Course course) {
        var courseDTO = modelMapper.map(course, CourseDTO.class);
        courseDTO.setStudentList(convert(course.getStudents()));
        return courseDTO;
    }
}
