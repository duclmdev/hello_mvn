package com.hello.mvn.utils;

import lombok.*;
import lombok.experimental.Accessors;

@Data
@Accessors(fluent = true)
public class Property {

    private Class<?> clazz;
    private String displayName;
    private String tableAlias;
    private String column;
    private Type type;

    @Getter
    @AllArgsConstructor
    public enum Type {
        NUMBER,
        STRING,
        OPTION
    }
}
