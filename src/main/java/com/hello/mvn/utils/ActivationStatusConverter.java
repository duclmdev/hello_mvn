package com.hello.mvn.utils;

import com.hello.mvn.entity.ActivationStatus;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter(autoApply = true)
public class ActivationStatusConverter implements AttributeConverter<ActivationStatus, Integer> {

    @Override
    public Integer convertToDatabaseColumn(ActivationStatus status) {
        return status == null ? null : status.getCode();
    }

    @Override
    public ActivationStatus convertToEntityAttribute(Integer code) {
        return ActivationStatus.fromCode(code);
    }
}
