package com.hello.mvn.utils;

import com.hello.mvn.dto.response.StudentDTO;
import com.hello.mvn.entity.StudentCourseDetail;
import org.modelmapper.AbstractConverter;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.List;

public class StudentListConverter extends AbstractConverter<List<StudentCourseDetail>, List<StudentDTO>> {

    private final ModelMapper modelMapper = new ModelMapper();

    @Override
    protected List<StudentDTO> convert(List<StudentCourseDetail> users) {
        List<StudentDTO> list = new ArrayList<>();
        for (StudentCourseDetail e : users) {
            StudentDTO map = modelMapper.map(e.getStudent(), StudentDTO.class);
            list.add(map);
        }
        return list;
    }
}
