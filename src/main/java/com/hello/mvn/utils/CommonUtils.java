package com.hello.mvn.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.beans.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CommonUtils {

    public static <T> T mergeObject(T target, T source) throws IntrospectionException, InvocationTargetException, IllegalAccessException {

        BeanInfo beanInfo = Introspector.getBeanInfo(source.getClass());

        for (PropertyDescriptor pd : beanInfo.getPropertyDescriptors()) {

            Method readMethod = pd.getReadMethod();
            Method writeMethod = pd.getWriteMethod();

            if (readMethod != null && !"class".equals(pd.getName())) {
                Object value = readMethod.invoke(source);

                if (value != null) {
                    writeMethod.invoke(target, value);
                }
            }
        }

        return target;
    }

}
