package com.hello.mvn.utils.annotation;

import java.lang.annotation.*;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface EntityMapping {

    String value();

    String searchable() default "";
}
