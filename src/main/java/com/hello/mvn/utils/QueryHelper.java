package com.hello.mvn.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.EMPTY;

public class QueryHelper {

    public static String convertToBooleanModeQuery(String searchTerm) {
        if (StringUtils.isBlank(searchTerm)) {
            return null;
        }
        return Arrays.stream(removeSpecialCharacters(searchTerm).split("\\s+"))
                .map(keyword -> "+" + keyword + "*")
                .collect(Collectors.joining(" "));
    }

    public static String convertToLikeOperatorQuery(String searchTerm) {
        return "%" + searchTerm + "%";
    }

    public static String normalizeDataForIndexing(String... strings) {
        return removeSpecialCharacters(joinNonNullsOnly(" ", strings));
    }

    public static String joinNonNullsOnly(String delimiter, String... strings) {
        return Arrays.stream(strings)
                .filter(Objects::nonNull)
                .collect(Collectors.joining(delimiter));
    }

    private static String removeSpecialCharacters(String string) {
        if (StringUtils.isBlank(string)) {
            return EMPTY;
        }
        String specialChars = "[^a-zA-Z0-9 ]";
        return string.replaceAll(specialChars, "");
    }
}
