package com.hello.mvn.utils;

import lombok.*;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PageableBuilder {

    private Integer page;

    private Integer size;

    private String sort;

    public Pageable build() {
        return build(null);
    }

    public Pageable build(Class<?> entityClass) {
        if (page == null) {
            page = 0;
        }

        if (size == null) {
            size = 10;
        }

        if (StringUtils.hasText(sort)) {
            var propertyMap = entityClass == null ? null : PropertyMapping.load(entityClass);
            return PageRequest.of(page, size, getSort(propertyMap));
        }

        return PageRequest.of(page, size);
    }

    private Sort getSort(PropertyMapping propertyMap) {
        String[] sortList = sort.split(",");

        List<Order> orders = new ArrayList<>();
        for (String sortInfo : sortList) {
            String[] sortArr = sortInfo.split(":");

            if (!StringUtils.hasText(sortArr[0])) {
                continue;
            }

            String property = getMappingColumn(sortArr[0], propertyMap);
            if (sortArr.length == 2 && StringUtils.hasText(sortArr[1])) {
                Direction direction = Direction.fromString(sortArr[1].trim());
                orders.add(new Order(direction, property));
            } else {
                orders.add(Order.asc(property));
            }
        }
        return Sort.by(orders);
    }

    private String getMappingColumn(String property, PropertyMapping propertyMap) {
        if (propertyMap == null) return property;

        var column = propertyMap.get(property.trim());
        return column == null ? property : column.column();
    }
}
