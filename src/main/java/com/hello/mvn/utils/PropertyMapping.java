package com.hello.mvn.utils;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.Map;

@Data
@Slf4j
@Accessors(fluent = true)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PropertyMapping {

    private static final Map<Class<?>, PropertyMapping> CACHE = new HashMap<>();
    private static final PropertyMapping instance = new PropertyMapping();

    private String className;
    private Class<?> entityClass;
    private String searchable;
    private String alias;
    private Map<String, Property> propertyMap = new HashMap<>();

    public static Map<Class<?>, PropertyMapping> getCache() {
        return CACHE;
    }

    public static PropertyMapping get(Class<?> entityClass) {
        return CACHE.get(entityClass);
    }

    public static PropertyMapping load(Class<?> entityClass) {
        if (entityClass == null) return null;

        var classMapping = CACHE.get(entityClass);
        if (classMapping == null) {
            log.info("loading {}", entityClass);
            classMapping = readClass(entityClass);
        }
        return classMapping;
    }

    private static PropertyMapping readClass(Class<?> entityClass) {
        var table = entityClass.getAnnotation(Table.class);
        var classMapping = new PropertyMapping()
                .entityClass(entityClass)
                .alias(table == null ? entityClass.getSimpleName() : table.name())
                .className(entityClass.getName());
        CACHE.put(entityClass, classMapping); // prevent infinite recursion
        var columns = readColumns(classMapping, entityClass);
        var superclass = entityClass.getSuperclass();
        if (superclass != null && superclass != Object.class) {
            columns.putAll(load(superclass).propertyMap);
        }
        CACHE.put(entityClass, classMapping.propertyMap(columns));
        return classMapping;
    }

    private static Map<String, Property> readColumns(PropertyMapping classMapping, Class<?> entityClass) {
        var map = new HashMap<String, Property>();

        for (var field : entityClass.getDeclaredFields()) {
            var fieldName = field.getName();
            var column = field.getAnnotation(Column.class);
            var columnName = column != null ? getColumnName(column, fieldName) : readJoinTable(field);
            var type = getType(field.getType());
            map.put(fieldName, new Property().tableAlias(classMapping.alias())
                    .clazz(field.getType())
                    .column(columnName)
                    .type(type));
        }
        return map;
    }

    private static String getColumnName(Column column, String fieldName) {
        String columnName = column.name();
        return columnName == null || columnName.isBlank() ? fieldName : column.name();
    }

    private static Property.Type getType(Class<?> fieldClass) {
        if (fieldClass.isAssignableFrom(String.class)) {
            return Property.Type.STRING;
        }
        if (isNumber(fieldClass)) {
            return Property.Type.NUMBER;
        }
        if (fieldClass.isEnum()) {
            return Property.Type.OPTION;
        }
        return null;
    }

    private static boolean isNumber(Class<?> fieldClass) {
        return Number.class.isAssignableFrom(fieldClass)
                || fieldClass == int.class //
                || fieldClass == double.class //
                || fieldClass == float.class;
    }

    private static String readJoinTable(Field field) {
        if (hasAnnotation(field, OneToMany.class)) {
            return readOneToMany(field);
        }
        if (hasAnnotation(field, ManyToOne.class) || hasAnnotation(field, OneToOne.class)) {
            return readReference(field);
        }
        return field.getName();
    }

    private static boolean hasAnnotation(Field field, Class<? extends Annotation> annotation) {
        return field.getAnnotation(annotation) != null;
    }

    private static String readReference(Field field) {
        // log.debug("reference {} -> {}\n", field.getName(), field.getType());
        load(field.getType());
        return field.getName();
    }

    private static String readOneToMany(Field field) {
        // log.debug("one to many {} -> {}\n", field.getName(), getGenericType(field));
        load(getGenericType(field));
        return field.getName();
    }

    private static Class<?> getGenericType(Field field) {
        var genericType = (ParameterizedType) field.getGenericType();
        var typeArguments = genericType.getActualTypeArguments();
        return (Class<?>) typeArguments[0];
    }

    public Property get(String prop) {
        return propertyMap.get(prop);
    }
}
