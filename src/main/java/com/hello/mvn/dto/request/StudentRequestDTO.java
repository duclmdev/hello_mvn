package com.hello.mvn.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hello.mvn.entity.Student;
import lombok.*;

import java.time.ZonedDateTime;

/** Request DTO for entity {@link Student} */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentRequestDTO {

    @JsonProperty
    private String name;

    @JsonProperty
    private ZonedDateTime dob;

    @JsonProperty
    private ContactInfoDTO contact;
}
