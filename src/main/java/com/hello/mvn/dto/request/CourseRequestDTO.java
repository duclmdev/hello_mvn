package com.hello.mvn.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hello.mvn.entity.ActivationStatus;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CourseRequestDTO {

    @JsonProperty
    private String name;

    @JsonProperty
    private String description;

    @JsonProperty
    private ActivationStatus status;
}
