package com.hello.mvn.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hello.mvn.entity.ActivationStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class StudentCourseDetailDTO {

    @JsonProperty("student")
    private StudentDTO student;

    @JsonProperty("course")
    private CourseDTO course;

    @JsonProperty
    private Integer rating;

    @JsonProperty("activation_status")
    private ActivationStatus status;

}
