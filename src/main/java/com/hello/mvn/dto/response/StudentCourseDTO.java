package com.hello.mvn.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class StudentCourseDTO {

    @JsonProperty("student_id")
    private Integer studentId;

    @JsonProperty("course_id")
    private Integer courseId;

}
