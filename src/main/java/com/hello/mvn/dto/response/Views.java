package com.hello.mvn.dto.response;

public interface Views {

    interface Public {}

    interface Internal extends Public {}
}
