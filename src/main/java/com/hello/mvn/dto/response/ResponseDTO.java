package com.hello.mvn.dto.response;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.hello.mvn.utils.ZonedDateTimeDeserializer;
import com.hello.mvn.utils.ZonedDateTimeSerializer;
import jakarta.servlet.http.HttpServletRequest;
import lombok.*;
import lombok.Builder.Default;
import lombok.experimental.Accessors;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.ZonedDateTime;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@JsonView(Views.Public.class)
public class ResponseDTO<T> {

    public static final String X_AMZN_REQUESTID = "x-amzn-requestid";

    @Default
    private static final Status SUCCESS = status(HttpStatus.OK);

    @JsonProperty
    private Header header;

    @JsonProperty
    private T body;

    @Default
    @JsonProperty
    private Status status = SUCCESS;

    public static String getRequestId(HttpServletRequest request) {
        String requestId = null;

        if (request != null) {
            requestId = request.getHeader(X_AMZN_REQUESTID);
        }

        return requestId == null ? UUID.randomUUID().toString() : requestId;
    }

    public static Status status(HttpStatus status) {
        return new Status(status);
    }

    public static <T> ResponseDTO<T> build(HttpServletRequest request, T body, Status status) {
        return new ResponseDTO<T>()
                .setBody(body)
                .setStatus(status)
                .setHeader(Header.builder(request).build());
    }

    public static <T> ResponseDTO<T> build(HttpServletRequest request, T body) {
        return build(request, body, HttpStatus.OK);
    }

    public static <T> ResponseDTO<T> build(HttpServletRequest request, T body, HttpStatus httpStatus) {
        return build(request, body, status(httpStatus));
    }

    public static ResponseDTO<EmptyBody> build(HttpServletRequest request) {
        return build(request, EmptyBody.INSTANCE, HttpStatus.OK);
    }

    public static ResponseDTO<EmptyBody> build(HttpServletRequest request, HttpStatus httpStatus) {
        return build(request, EmptyBody.INSTANCE, httpStatus);
    }

    @Deprecated(forRemoval = true)
    public static ResponseDTOBuilder<EmptyBody> emptyBodyBuilder(HttpServletRequest request) {
        return new ResponseDTOBuilder<EmptyBody>()
                .header(Header.builder(request).build())
                .body(EmptyBody.INSTANCE);
    }

    public ResponseEntity<ResponseDTO<T>> toResponseEntity() {
        return new ResponseEntity<>(this, this.getStatus().httpStatus);
    }

    public ResponseEntity<ResponseDTO<T>> toResponseEntity(HttpHeaders headers) {
        return new ResponseEntity<>(this, headers, this.getStatus().httpStatus);
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @JsonView(Views.Public.class)
    public static class Header {

        @JsonProperty
        private String requestId;

        @Default
        @JsonProperty
        @JsonSerialize(using = ZonedDateTimeSerializer.class)
        @JsonDeserialize(using = ZonedDateTimeDeserializer.class)
        private ZonedDateTime timestamp = ZonedDateTime.now();

        public static HeaderBuilder builder(HttpServletRequest request) {
            return new HeaderBuilder().requestId(ResponseDTO.getRequestId(request));
        }
    }

    @Data
    @JsonSerialize
    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class EmptyBody {

        public static final EmptyBody INSTANCE = new EmptyBody();
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    @JsonView(Views.Public.class)
    @JsonInclude(Include.NON_NULL)
    public static class Status {

        @JsonIgnore
        private HttpStatus httpStatus;

        @JsonProperty
        private int statusCode;

        @JsonProperty
        private String type;

        @JsonProperty
        private String title;

        @JsonProperty
        private String detail;

        @JsonProperty
        private String instance;

        public Status(HttpStatus status) {
            httpStatus = status;
            statusCode = status.value();
            title = status.getReasonPhrase();
        }
    }
}

