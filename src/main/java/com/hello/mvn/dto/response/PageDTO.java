package com.hello.mvn.dto.response;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import java.util.List;

@Data
@Builder
@JsonView(Views.Public.class)
public class PageDTO<T> {

    private List<T> content;
    private PageableDTO pageable;

    public static <S> PageDTO<S> from(Page<S> page) {
        return builder(page).build();
    }

    public static <S> PageDTO.PageDTOBuilder<S> builder(Page<S> page) {
        return new PageDTOBuilder<S>() //
                .content(page.getContent()) //
                .pageable(PageableDTO.from(page));
    }

    @Data
    @Builder
    public static class PageableDTO {
        private boolean first;
        private boolean last;

        private int size;
        private int page;

        private int numberOfElements;
        private int totalPages;
        private long totalElements;

        private Sort sort;

        public static <T> PageableDTO from(Page<T> page) {
            return PageableDTO.builder() //
                    .first(page.isFirst()) //
                    .last(page.isLast()) //
                    .size(page.getSize()) //
                    .page(page.getNumber()) //
                    .numberOfElements(page.getNumberOfElements()) //
                    .totalPages(page.getTotalPages()) //
                    .totalElements(page.getTotalElements()) //
                    .sort(page.getSort()) //
                    .build();
        }
    }
}
