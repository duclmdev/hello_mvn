package com.hello.mvn.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;

import java.time.ZonedDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonView(Views.Public.class)
public class StudentDTO {

    @JsonProperty
    private Integer id;

    @JsonProperty
    private String name;

    @JsonProperty
    @JsonView(Views.Internal.class)
    private ZonedDateTime dob;

    @JsonProperty
    private ContactDTO contact;

    @JsonProperty
    private List<CourseDTO> courseList;
}
