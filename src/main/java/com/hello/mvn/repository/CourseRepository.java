package com.hello.mvn.repository;

import com.hello.mvn.entity.Course;
import com.hello.mvn.entity.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CourseRepository extends JpaRepository<Course, Integer> {

    //language=mysql
    String SELECT = """
            SELECT
              c.id as cid, c.name, c.activation_status, c.description, c.created_by, c.created_timestamp, c.updated_by,
              c.updated_timestamp, course_id, student_id, rating, scd.activation_status as status, s.id, s.name as s_name,
              dob, latest_contact_id
            FROM course c
                JOIN student_course_detail scd ON c.id = scd.course_id
                JOIN student s ON s.id = scd.student_id
            """;

    String CRITERIA = """
            WHERE MATCH (c.name) AGAINST (:query IN BOOLEAN MODE) AND c.activation_status = :status
            """;

    String STUDENT_CRITERIA = "WHERE c.id = :id";

    @Query(value = SELECT + CRITERIA, nativeQuery = true)
    Page<Course> search(@Param("query") String query, @Param("status") Integer status, Pageable build);

    @Query(value = SELECT + STUDENT_CRITERIA, nativeQuery = true)
    Page<Student> getStudents(@Param("id") Integer courseId, Pageable build);
}
