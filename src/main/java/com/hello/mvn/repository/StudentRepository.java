package com.hello.mvn.repository;

import com.hello.mvn.entity.Student;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface StudentRepository extends JpaRepository<Student, Integer> {

    //language=mysql
    String SELECT = """
        SELECT s.*, ci.id ci_id, ci.address, ci.email, ci.phone_number, ci.student_id
        FROM student s
            JOIN contact_info ci ON ci.id = s.latest_contact_id
            JOIN student_course_detail scd ON s.id = scd.student_id
            JOIN course c ON c.id = scd.course_id
        """;

    String CRITERIA = """
            WHERE (:query IS NULL OR MATCH(s.name) AGAINST (:query IN BOOLEAN MODE))
              AND (:status IS NULL OR scd.activation_status = :status)
            """;

    String GROUP_BY = """
            GROUP BY s.id
            """;

    @Query(value = SELECT + CRITERIA + GROUP_BY, nativeQuery = true)
    Page<Student> search(@Param("query") String query, @Param("status") Integer status, Pageable build);
}
