package com.hello.mvn.service;

import com.hello.mvn.entity.*;
import com.hello.mvn.service.impl.CrudService;
import com.hello.mvn.utils.PageableBuilder;
import org.springframework.data.domain.Page;

import java.util.Set;

public interface CourseService extends CrudService<Course, Integer> {

    Page<Course> search(String query, Set<ActivationStatus> statusSet, PageableBuilder pageable);

    Page<Student> getStudents(Integer courseId, PageableBuilder pageable);

    StudentCourseDetail register(Integer courseId, Integer studentId);
}
