package com.hello.mvn.service.impl;

import com.hello.mvn.entity.ActivationStatus;
import com.hello.mvn.entity.Student;
import com.hello.mvn.exception.ResourceNotFoundException;
import com.hello.mvn.exception.ServiceException;
import com.hello.mvn.repository.StudentRepository;
import com.hello.mvn.service.ContactInfoService;
import com.hello.mvn.service.StudentService;
import com.hello.mvn.utils.PageableBuilder;
import com.hello.mvn.utils.QueryHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Set;

@Slf4j
@Service
@RequiredArgsConstructor
public class StudentServiceImpl extends AbstractCrudService<Student, Integer, StudentRepository>
        implements StudentService {

    private final ContactInfoService contactInfoService;

    @Override
    public Student create(Student student) throws ServiceException {
        var contactInfo = student.getContactInfo();
        System.out.println(contactInfo);
        contactInfo.setStudent(student);
        return super.create(student);
    }

    @Override
    public Student updateById(Integer id, Student student) throws ServiceException, ResourceNotFoundException {
        student.setId(id);
        return super.updateById(id, student);
    }

    @Override
    public Page<Student> search(String query, Set<ActivationStatus> statusSet, PageableBuilder pageable) {
        var status = ActivationStatus.fromSet(statusSet);
        query = QueryHelper.convertToBooleanModeQuery(query);
        var code = status == null ? null : status.getCode();
        log.info("query={}; status code={}", query, code);
        return repository.search(query, code, pageable.build());
    }
}
