package com.hello.mvn.service.impl;

import com.hello.mvn.exception.ResourceNotFoundException;
import com.hello.mvn.exception.ServiceException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CrudService<T, I> {

    T create(T entity) throws ServiceException;

    T getById(I id) throws ResourceNotFoundException, ServiceException;

    Page<T> getByPage(Pageable pageable) throws ServiceException;

    T updateById(I id, T entity) throws ResourceNotFoundException, ServiceException;

    void deleteById(I id) throws ResourceNotFoundException, ServiceException;
}
