package com.hello.mvn.service.impl;

import com.hello.mvn.entity.*;
import com.hello.mvn.exception.ResourceNotFoundException;
import com.hello.mvn.exception.ServiceException;
import com.hello.mvn.repository.CourseRepository;
import com.hello.mvn.service.CourseService;
import com.hello.mvn.utils.PageableBuilder;
import com.hello.mvn.utils.QueryHelper;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class CourseServiceImpl extends AbstractCrudService<Course, Integer, CourseRepository> implements CourseService {

    @Override
    public Page<Course> search(String query, Set<ActivationStatus> statusSet, PageableBuilder pageable) {
        var status = ActivationStatus.fromSet(statusSet);
        var code = status == null ? null : status.getCode();
        query = QueryHelper.convertToBooleanModeQuery(query);
        log.info("query={}; status code={}", query, code);
        return repository.search(query, code, pageable.build());
    }

    @Override
    public Page<Student> getStudents(Integer courseId, PageableBuilder pageable) {
        return repository.getStudents(courseId, pageable.build());
    }

    @Override
    public Course updateById(Integer id, Course course) throws ServiceException, ResourceNotFoundException {
        course.setId(id);
        return super.updateById(id, course);
    }

    @Override
    public StudentCourseDetail register(Integer courseId, Integer studentId) {
        var detail = StudentCourseDetail.builder()
                .id(new StudentCourseDetail.PK(courseId, studentId))
                .status(ActivationStatus.ACTIVE)
                .build();
        return null; // repository.save(detail);
    }
}
