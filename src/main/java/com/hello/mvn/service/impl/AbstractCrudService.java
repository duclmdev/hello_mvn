package com.hello.mvn.service.impl;

import com.hello.mvn.exception.ResourceNotFoundException;
import com.hello.mvn.exception.ServiceException;
import com.hello.mvn.utils.CommonUtils;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

@Setter
public abstract class AbstractCrudService<T, I, R extends JpaRepository<T, I>> implements CrudService<T, I> {

    protected final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    protected R repository;

    @Override
    public T create(T entity) throws ServiceException {
        log.debug("create: {}", entity);

        try {
            return repository.save(entity);
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public T getById(I id) throws ResourceNotFoundException, ServiceException {
        log.debug("get by id: {}", id);

        try {
            var entity = repository.findById(id);
            if (entity.isPresent()) {
                return entity.get();
            }
        } catch (Exception e) {
            throw new ServiceException(e);
        }
        throw new ResourceNotFoundException();
    }

    @Override
    public Page<T> getByPage(Pageable pageable) throws ServiceException {
        log.debug("get by page: {}", pageable);

        try {
            return repository.findAll(pageable);
        } catch (Exception e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public T updateById(I id, T entity) throws ResourceNotFoundException, ServiceException {
        log.debug("update by id: {} -> {}", id, entity);

        try {
            Optional<T> optional = repository.findById(id);
            if (optional.isPresent()) {
                T current = optional.get();
                T updated = CommonUtils.mergeObject(current, entity);
                return repository.save(updated);
            }
        } catch (Exception e) {
            throw new ServiceException(e);
        }
        throw new ResourceNotFoundException();
    }

    @Override
    public void deleteById(I id) throws ResourceNotFoundException, ServiceException {
        log.debug("delete by id: {}", id);

        try {
            var entity = repository.findById(id);
            if (entity.isPresent()) {
                repository.delete(entity.get());
                return;
            }
        } catch (Exception e) {
            throw new ServiceException(e);
        }
        throw new ResourceNotFoundException();
    }
}
