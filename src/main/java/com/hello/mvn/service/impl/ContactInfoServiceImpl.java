package com.hello.mvn.service.impl;

import com.hello.mvn.entity.ContactInfo;
import com.hello.mvn.exception.ResourceNotFoundException;
import com.hello.mvn.exception.ServiceException;
import com.hello.mvn.repository.ContactInfoRepository;
import com.hello.mvn.service.ContactInfoService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ContactInfoServiceImpl extends AbstractCrudService<ContactInfo, Integer, ContactInfoRepository>
        implements ContactInfoService {

    @Override
    public ContactInfo updateById(
            Integer id, ContactInfo contactInfo
    ) throws ServiceException, ResourceNotFoundException {
        contactInfo.setId(id);
        return super.updateById(id, contactInfo);
    }
}
