package com.hello.mvn.service;

import com.hello.mvn.entity.ActivationStatus;
import com.hello.mvn.entity.Student;
import com.hello.mvn.service.impl.CrudService;
import com.hello.mvn.utils.PageableBuilder;
import org.springframework.data.domain.Page;

import java.util.Set;

public interface StudentService extends CrudService<Student, Integer> {

    Page<Student> search(String query, Set<ActivationStatus> statusSet, PageableBuilder pageable);
}
