package com.hello.mvn.service;

import com.hello.mvn.entity.ContactInfo;
import com.hello.mvn.service.impl.CrudService;

public interface ContactInfoService extends CrudService<ContactInfo, Integer> {
}
