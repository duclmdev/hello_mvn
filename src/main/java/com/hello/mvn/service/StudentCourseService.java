package com.hello.mvn.service;

import com.hello.mvn.entity.StudentCourseDetail;

public interface StudentCourseService {

    StudentCourseDetail register(Integer courseId, Integer studentId);
}
