package com.hello.mvn.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
public class ServiceException extends Exception implements Serializable {

    private final Exception e;
}
