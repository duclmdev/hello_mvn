package com.hello.mvn.exception;

public class ResourceExistedException extends Exception {

    public ResourceExistedException() {
        super("Resource existed");
    }

    public ResourceExistedException(String message) {
        super(message);
    }
}
