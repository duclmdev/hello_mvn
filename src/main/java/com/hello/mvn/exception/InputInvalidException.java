package com.hello.mvn.exception;

public class InputInvalidException extends Exception{
    public InputInvalidException() {
        super("Invalid input!");
    }
    public InputInvalidException(String message){
        super(message);
    }
}
