package com.hello.mvn.entity;

import com.hello.mvn.utils.annotation.EntityMapping;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import java.time.ZonedDateTime;
import java.util.List;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(schema = "student")
@EntityMapping(value = "s", searchable = "name")
public class Student extends Auditable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "dob")
    private ZonedDateTime dateOfBirth;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "latest_contact_id", referencedColumnName = "id")
    private ContactInfo contactInfo;

    @Fetch(FetchMode.SUBSELECT)
    @OneToMany(mappedBy = "student", fetch = FetchType.LAZY)
    private List<StudentCourseDetail> courses;
}
