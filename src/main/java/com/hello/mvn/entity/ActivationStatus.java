package com.hello.mvn.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Set;

@Getter
@AllArgsConstructor
public enum ActivationStatus {

    ACTIVE(0), INACTIVE(1);

    private final Integer code;

    public static ActivationStatus fromCode(Integer code) {
        return switch (code) {
            case 0 -> ACTIVE;
            case 1 -> INACTIVE;
            default -> null;
        };
    }

    public static ActivationStatus fromSet(Set<ActivationStatus> statusSet) {
        if (statusSet == null || statusSet.size() != 1) {
            return null;
        }
        return statusSet.contains(ACTIVE) ? ACTIVE : INACTIVE;
    }
}
