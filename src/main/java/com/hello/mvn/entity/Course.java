package com.hello.mvn.entity;

import com.hello.mvn.utils.annotation.EntityMapping;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import java.util.List;

@Getter
@Setter
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(schema = "course")
@EntityMapping(value = "c", searchable = "name")
public class Course extends Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "activation_status")
    private ActivationStatus status;

    @Fetch(FetchMode.SUBSELECT)
    @OneToMany(mappedBy = "course", fetch = FetchType.LAZY)
    private List<StudentCourseDetail> students;
}
